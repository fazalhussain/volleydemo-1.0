package com.volleydemo;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.service.voice.VoiceInteractionSession;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

import static com.android.volley.Request.Method.GET;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final TextView res = (TextView) findViewById(R.id.textViewtext);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }
        });
                ((Button) findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = "http://httpbin.org/html";
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                res.setText(response);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                showtoast(error.toString());
                            }
                        });
                        // showtoast("jhhgkj");

                        Volley.newRequestQueue(MainActivity.this).add(stringRequest);
                    }
                });



        ((Button) findViewById(R.id.buttonobjReq)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://httpbin.org/get?site=code&network=tutsplus";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            response = response.getJSONObject("headers");
                            //String str1 = "Network: "+response.getString("network")+"\nSite: "+response.getString("site");

                            Gson gson = new Gson();
                            Header header = gson.fromJson(response.toString(), Header.class);
                            Global global = gson.fromJson(response.toString(), Global.class);
                            res.setText(header.AcceptEnoding);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showtoast(error.toString());
                    }
                });
                Volley.newRequestQueue(MainActivity.this).add(jsonObjectRequest);
            }

        });

             final ImageView imageView = (ImageView) findViewById(R.id.imageView);
                     ((Button) findViewById(R.id.buttonimgreq)).setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     String url = "http://i.imgur.com/Nwk25LA.jpg";
                     ImageRequest imageRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {
                         @Override
                         public void onResponse(Bitmap response) {
                           imageView.setImageBitmap(response);
                         }
                     }, 0, 0, ImageView.ScaleType.FIT_XY, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
                         @Override
                         public void onErrorResponse(VolleyError error) {
                             imageView.setBackgroundColor(Color.parseColor("#ff0000"));
                             showtoast("Parsing Error: " + error.toString());
                         }
                     });
                     Volley.newRequestQueue(MainActivity.this).add(imageRequest);
                 }
             });

    }
    private void showtoast(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
