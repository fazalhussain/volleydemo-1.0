package com.volleydemo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Acer on 11/4/2015.
 */
public class Header {
    String Accept;
    String Host;
    @SerializedName("Accept-Encoding")
    String AcceptEnoding;

    public String getAccept() {
        return Accept;
    }

    public void setAccept(String accept) {
        Accept = accept;
    }

    public String getAcceptEnoding() {
        return AcceptEnoding;
    }

    public void setAcceptEnoding(String acceptEnoding) {
        AcceptEnoding = acceptEnoding;
    }

    public String getHost() {
        return Host;
    }

    public void setHost(String host) {
        Host = host;
    }
}
