package com.volleydemo;

/**
 * Created by Acer on 11/4/2015.
 */
public class Myargs {
    String network;
    String site;

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
