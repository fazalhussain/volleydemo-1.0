package com.volleydemo;

/**
 * Created by Acer on 11/4/2015.
 */
public class Global {
    String origin;
    String url;
    Myargs myargs;
    Header header;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Myargs getMyargs() {
        return myargs;
    }

    public void setMyargs(Myargs myargs) {
        this.myargs = myargs;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }
}
